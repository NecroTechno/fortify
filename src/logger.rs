use crate::util::Status;
use std::io;
use std::io::Write;
use std::process::exit;

pub enum Colour {
    Red,
    Yellow,
    Green,
    LBlue,
    Orange,
}

pub struct Logger {
    pub plain_output_mode: bool,
}

impl Logger {
    pub fn underline(&self) {
        if self.plain_output_mode {
            return;
        }
        for _i in 0..34 {
            print!("-");
        }
        println!("-");
    }

    pub fn nl(&self) {
        if self.plain_output_mode {
            return;
        }
        println!();
    }

    pub fn print_header(&self, header_str: &str) {
        if self.plain_output_mode {
            return;
        }
        print!("[*] ");
        self.print_colour(header_str, Colour::LBlue, true);
        self.underline();
        self.nl();
    }

    pub fn print_colour(&self, print_arg: &str, colour: Colour, newline: bool) {
        let newline_char = match newline {
            true => "\n",
            false => "",
        };

        if self.plain_output_mode {
            print!("{}{}", print_arg, newline_char);
            return;
        }

        match colour {
            Colour::Red => print!("\x1b[0;31m{}\x1b[0m{}", print_arg, newline_char),
            Colour::Yellow => print!("\x1b[1;33m{}\x1b[0m{}", print_arg, newline_char),
            Colour::Green => print!("\x1b[0;32m{}\x1b[0m{}", print_arg, newline_char),
            Colour::LBlue => print!("\x1b[1;34m{}\x1b[0m{}", print_arg, newline_char),
            Colour::Orange => print!("\x1b[0;33m{}\x1b[0m{}", print_arg, newline_char),
        }
    }

    fn task_status_border_left(&self) {
        if self.plain_output_mode {
            return;
        }
        print!("|: ");
    }

    fn task_status_border_right(&self) {
        if self.plain_output_mode {
            return;
        }
        print!(" :|");
    }

    pub fn update_task_status(&self, task_str: &str, updated_task_status: Status) {
        print!("\r");
        self.task_status(task_str, updated_task_status);
        self.nl();
    }

    pub fn task_status(&self, task_str: &str, task_status: Status) {
        if self.plain_output_mode && task_status == Status::InProg {
            return;
        }
        print!("{}", task_str);

        if !self.plain_output_mode {
            for _i in 0..(50 - task_str.len()) {
                print!(" ");
            }
        } else {
            print!(" ");
        }
        match task_status {
            Status::Okay => {
                self.task_status_border_left();
                self.print_colour("OKAY", Colour::Green, false);
                self.task_status_border_right();
            }
            Status::InProg => {
                self.task_status_border_left();
                self.print_colour("PROG", Colour::Yellow, false);
                self.task_status_border_right();
            }
            Status::Warn => {
                self.task_status_border_left();
                self.print_colour("WARN", Colour::Orange, false);
                self.task_status_border_right();
            }
            Status::Fail => {
                self.task_status_border_left();
                self.print_colour("FAIL", Colour::Red, false);
                self.task_status_border_right();
            }
        }
        io::stdout()
            .flush()
            .expect("Unable to flush output. Exiting.");
        if self.plain_output_mode {
            println!();
        }
    }

    // this needs to be collated into a log!
    pub fn inform_user(&self, info_vec: Vec<&str>) {
        for info in info_vec {
            if !self.plain_output_mode {
                print!("\x1b[1mINFO: \x1b[0m");
            } else {
                print!("INFO: ");
            }
            println!("{}", info);
        }
    }

    pub fn request_continue(&self) {
        if self.plain_output_mode {
            return;
        }
        print!("Press [y] to continue, [n] to end: ");
        io::stdout()
            .flush()
            .expect("Unable to flush output. Exiting.");
        let mut input = String::new();
        std::io::stdin()
            .read_line(&mut input)
            .expect("Unable to read input. Exiting.");
        let option: u8 = input
            .bytes()
            .next()
            .expect("Unable to read input. Exiting.") as u8;
        match option {
            121 => {}
            110 => {
                self.nl();
                println!("Exiting.");
                exit(0);
            }
            _ => {
                println!("Unable to interpret input.");
                self.request_continue();
            }
        }
    }

    pub fn root_required(&self, module_title: &str) {
        self.inform_user(vec![&format!("Module '{}' requires root.", module_title)]);
    }
}
