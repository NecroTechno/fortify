use crate::logger;
use crate::util::{parse_command_output, Status};
use std::process::exit;
use std::process::Command;

use nix::unistd::Uid;

struct Auditor {
    // list of required programs and command
    deps: Vec<String>,
    is_root: bool,
    yes_mode: bool,
    logger: logger::Logger,
    inform_user_strings: Vec<String>,
}

impl Auditor {
    fn handle_info(&mut self, status: Status, info: Vec<&str>) {
        if status != Status::Okay {
            for i in info {
                self.inform_user_strings.push(i.to_string().clone());
            }
        }
    }

    fn review_deps(&mut self) {
        self.logger.print_header("Dependency Check");

        let mut cont = true;

        for dep in &self.deps {
            let task_str = &format!("Checking dependancy {}...", dep);
            self.logger.task_status(task_str, Status::InProg);
            let run_command = Command::new("sh")
                .arg("-c")
                .arg(format!("command -v {}", dep))
                .output();
            match run_command {
                Ok(output) => {
                    if !output.stdout.is_empty() {
                        self.logger.update_task_status(task_str, Status::Okay);
                    } else {
                        cont = false;
                        self.logger.update_task_status(task_str, Status::Fail);
                    }
                }
                Err(_e) => panic!("Opening sh failed! Exiting."),
            }
        }

        if self.is_root {
            let su_commands = vec!["/sbin/sysctl"];

            for su_command in &su_commands {
                let su_task_str = &format!("Checking root dependancy {}...", su_command);
                self.logger.task_status(su_task_str, Status::InProg);
                let su_run_command = Command::new("sh")
                    .arg("-c")
                    .arg(format!("ls {}", su_command))
                    .output();
                match su_run_command {
                    Ok(output) => {
                        if !output.stdout.is_empty() {
                            self.logger.update_task_status(su_task_str, Status::Okay);
                        } else {
                            cont = false;
                            self.logger.update_task_status(su_task_str, Status::Fail);
                        }
                    }
                    Err(_e) => panic!("Opening sh failed! Exiting."),
                }
            }
        }

        if !cont {
            self.logger.nl();
            println!("Some dependencies are missing! Exiting.");
            self.logger.nl();
            exit(1);
        }
    }

    fn single_string_comp(
        &self,
        task_str: &str,
        command: &str,
        comp: &str,
        pos_comp: bool,
    ) -> Status {
        self.logger.task_status(task_str, Status::InProg);
        let run_command = Command::new("sh").arg("-c").arg(command).output();
        match run_command {
            Ok(output) => {
                let comp_closure = |a: &str, b: &str| {
                    if pos_comp {
                        a == b
                    } else {
                        a != b
                    }
                };
                let parsed = parse_command_output(output.stdout);
                if comp_closure(&parsed, comp) {
                    let updated_status = Status::Okay;
                    self.logger.update_task_status(task_str, updated_status);
                    updated_status
                } else {
                    let updated_status = Status::Warn;
                    self.logger.update_task_status(task_str, updated_status);
                    updated_status
                }
            }
            Err(_e) => panic!("Opening sh failed! Exiting."),
        }
    }

    fn kernel_security(&mut self) {
        self.logger.print_header("Kernel Security");
        if !self.is_root {
            self.logger.root_required("Kernel Security");
            return;
        }

        let kptr_restriction_result = self.single_string_comp(
            "Checking kernel pointer restriction...",
            "sysctl kernel.kptr_restrict | tail -n 1 | awk '{print $3}'",
            "2",
            true,
        );
        self.handle_info(
            kptr_restriction_result,
            vec![
                "Kernel pointers are not currently hidden.",
                "You can change this by running 'sysctl -w kernel.kptr_restrict=2'.",
            ],
        );

        let dmesg_restriction_result = self.single_string_comp(
            "Checking dmesg log restriction...",
            "sysctl kernel.dmesg_restrict | tail -n 1 | awk '{print $3}'",
            "1",
            true,
        );
        self.handle_info(
            dmesg_restriction_result,
            vec![
                "dmesg can leak kernel pointers.",
                "You can restrict this by running 'sysctl -w kernel.dmesg_restrict=1'.",
            ],
        );

        let tty_line_load_restriction_result = self.single_string_comp(
            "Checking tty line loading restriction...",
            "sysctl dev.tty.ldisc_autoload | tail -n 1 | awk '{print $3}'",
            "0",
            true,
        );
        self.handle_info(
            tty_line_load_restriction_result,
            vec![
                "Vulnerable line disciplines can be loaded with ioctl.",
                "You can restrict this by running 'sysctl -w dev.tty.ldisc_autoload=0'.",
            ],
        );

        let unprivileged_userfault_check_result = self.single_string_comp(
            "Checking for unprivileged userfaultfd() access...",
            "sysctl vm.unprivileged_userfaultfd | tail -n 1 | awk '{print $3}'",
            "0",
            true,
        );
        self.handle_info(
            unprivileged_userfault_check_result,
            vec![
                "Userfaultfd() can be used to exploit user-after-free bugs..",
                "You can restrict this by running 'sysctl -w vm.unprivileged_userfaultfd=0'.",
            ],
        );

        self.logger
            .inform_user(self.inform_user_strings.iter().map(|i| &**i).collect());

        self.inform_user_strings.clear();
    }

    fn account_security(&mut self) {
        self.logger.print_header("User Account Security");
        let faillog_result = self.single_string_comp(
            "Checking maximum user login attempts...",
            "faillog -u $USER | tail -n 1 | awk '{print $3}'",
            "0",
            false,
        );
        if faillog_result != Status::Okay {
            self.logger.inform_user(vec![
                "The current user account has no login failure limits.",
                "Please consult 'man faillog' for more info.",
            ]);
        }
    }

    fn audit(&mut self) {
        self.logger.nl();
        let mut audit_step = |step: &dyn Fn(&mut Auditor), should_request_cont: bool| {
            step(self);
            self.logger.nl();
            if !self.yes_mode && should_request_cont {
                self.logger.request_continue();
                self.logger.nl();
            }
        };

        // consider if steps need to be struct methods
        audit_step(&Auditor::review_deps, true);
        audit_step(&Auditor::kernel_security, true);
        audit_step(&Auditor::account_security, false);
    }
}

pub fn audit(plain_output_mode: bool, yes_mode: bool) {
    let mut auditor = Auditor {
        deps: vec!["faillog".to_string()],
        is_root: Uid::effective().is_root(),
        yes_mode,
        logger: logger::Logger { plain_output_mode },
        inform_user_strings: vec![],
    };

    auditor.audit();
}
