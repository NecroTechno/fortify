extern crate clap;
extern crate nix;

mod audit;
mod logger;
mod util;

use clap::{App, AppSettings, Arg};
use nix::unistd::Uid;
use std::process::exit;

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    let matches = App::new("fortify")
                        .version(VERSION)
                        .author("NecroTechno <necrotechno@riseup.net>")
                        .about("A tool to simplify Linux hardening.")
			.setting(AppSettings::ArgRequiredElseHelp)
			.setting(AppSettings::GlobalVersion)
                        .subcommand(App::new("audit")
                            .about("Produces a report detailing the current state of the tested machine with recommendations.")
                            .arg(Arg::new("plain")
                                .short('p')
                                .about("Allows machine readable output. (Will assume yes on request to continue.)"))
                            .arg(Arg::new("yes")
                                .short('y')
                                .about("Assume yes on the request to continue."))
                            )
                        .subcommand(App::new("firewall")
                            .about("A tool to assist in iptables configuration."))
                        .get_matches();

    if let Some(audit_command) = matches.subcommand_matches("audit") {
        //println!("{:?}", audit_command);
        audit::audit(
            audit_command.is_present("plain"),
            audit_command.is_present("yes"),
        );
    }

    if let Some(_matches) = matches.subcommand_matches("firewall") {
        if !Uid::effective().is_root() {
            println!("Audit and manipulation of iptables requires root. Exiting.");
            exit(13);
        }
        println!("firewall");
    }
}
