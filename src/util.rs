use std::process::Command;
use std::str;

#[derive(Copy, Clone, PartialEq)]
pub enum Status {
    Okay,
    InProg,
    Warn,
    Fail,
}

pub fn parse_command_output(stdout: Vec<u8>) -> String {
    str::from_utf8(&stdout).unwrap().replace("\n", "")
}

// NOT WORKING
/// A function for enabling and disabling user input.
/// true to enable, false to disable
#[allow(dead_code)]
pub fn toggle_user_input(switch: bool) {
    match switch {
        true => Command::new("sh")
            .arg("-c")
            .arg("stty echo")
            .output()
            .expect("Unable to interact with stty. Exiting."),
        false => Command::new("sh")
            .arg("-c")
            .arg("stty -echo")
            .output()
            .expect("Unable to interact with stty. Exiting."),
    };
}
